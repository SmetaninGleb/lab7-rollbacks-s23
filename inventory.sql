CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username),
    product TEXT REFERENCES Shop(product),
    amount INT CHECK(amount >= 0),
    PRIMARY KEY (username, product)
);
