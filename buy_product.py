import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
amount_in_inventory = "SELECT sum(amount) FROM Inventory WHERE username = %(username)s"
update_inventory ="INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="root",
        host="localhost",
        port=5432
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")   
            except psycopg2.errors.CheckViolation:
                raise Exception("Product is out of stock")

            cur.execute(amount_in_inventory, obj)
            res = cur.fetchone()
            if res is None:
                raise Exception("Cannot find user in inventory")
            current_amount = 0 if res[0] is None else res[0]
            if current_amount + amount > 100:
                raise Exception("Maximum number of products exceeded")
            
            cur.execute(update_inventory, obj)
            if cur.rowcount != 1:
                raise Exception("Failed to update inventory")
